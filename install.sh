#!/bin/bash

OLDIFS=$IFS
IFS=$(echo -en "\n\b")
if [ -e /usr/bin/sudo ] ; then
    SUDO="sudo"
else
    SUDO=""
fi
pack=0
RED="\\033[1;31m"
DEFAULT="\\033[0;39m"
PKG_INST="apt-get"


echo -e "*************** NovaLPDrv v1.0 ****************\n"
echo "This driver is distributed under the GNU GPL License"
echo -e "This license is available in the 'LICENSE' file\n"
ask="Did you read, understood, accepted this license ? [yes/no]"

while true; do
    read -p "$ask" ans
    case $ans in
        [yY]* )
            break;;
        * ) exit;
    esac
done

#******************************* PREREQUISITES ******************************************
function required {
    echo -e "\n=> Verify prerequisites :\n"

    if [ -e /usr/bin/gcc ] ; then
        echo -e "- gcc: \\033[1;32mok\\033[0;39m"
    else
        echo -e "- gcc: \\033[1;31mnok\\033[0;39m"
        echo -e "You need to install the gcc compiler (or build-essential if available)"
        echo "The following command should tell you the package to install:"
        echo -e "apt-cache search gcc | grep \"^gcc-\""
        exit
    fi
    if [ -e /lib/modules/$(uname -r)/build ] ; then
        echo -e "- linux headers: \\033[1;32mok\\033[0;39m\n"
    else
        echo -e "- linux headers: \\033[1;31mnok\\033[0;39m\n"
        echo -e "You need to install your kernel's headers"
        echo "The following command should tell you the package to install:"
        echo -e "apt-cache search header | grep $(uname -r)"
        exit
    fi
}
#yum update
#yum install -y gcc
#yum install -y $(yum search kernel-devel | grep.devel | grep -v "======" | cut -d ' ' -f1)

#yum install -y gstreamer-devel.i686
#yum install -y gstreamer-plugins-bad-free-devel.i686
#yum install -y alsa-lib-devel.i686
#yum install -y gstreamer-devel.i686


#******************************* COMPILATION ******************************************
if [ -e NovaLPDrv.ko ] ; then
    echo -e "\nDriver already created, skipping compilation.\n"
else
    required
    echo -e "=> Compiling the driver:\n"
    make
    if [ -e NovaLPDrv.ko ] ; then
        echo -e "\n=> Compilation finished with success\n"
    else
        echo -e "\nAn issue occured while compiling\n=> Stopping installation"
        exit
    fi
fi
#********************************** COPY ***********************************************
echo -e "=> Copying driver into '/lib/modules/$(uname -r)/kernel/drivers/usb/misc':"

eval "$SUDO cp NovaLPDrv.ko /lib/modules/$(uname -r)/kernel/drivers/usb/misc"
if [ $? = 1 ] ; then
    echo -e "\nProblem when copying... missing root/sudo rights?\nStopping installation"
    exit
fi
#********************************** DEPMOD *********************************************
echo -e "=> Updating kernel module list"
eval "$SUDO depmod"
content=$(grep NovaLPDrv /etc/modules)
if [[ "$content" = "" ]] ; then
    $SUDO sh -c "echo \"NovaLPDrv\" >> /etc/modules"
fi
#********************************** RULES *********************************************
echo -e "=> Adding launchpad plugging rules into '/etc/udev/rules.d'"

$SUDO sh -c 'echo SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"1235\", ATTRS{idProduct}==\"000e\", DRIVERS==\"NovaLPDrv\" > /etc/udev/rules.d/10-persistent-NovaLPDrv.rules'
$SUDO sh -c 'echo KERNEL==\"nlp[0-9]\", DRIVERS==\"NovaLPDrv\", MODE=\"0666\" >> /etc/udev/rules.d/10-persistent-NovaLPDrv.rules'
$SUDO sh -c 'echo SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"1235\", ATTRS{idProduct}==\"000e\", GROUP=\"plugdev\" >> /etc/udev/rules.d/10-persistent-NovaLPDrv.rules'

#********************************** MODULES *********************************************
echo -e "=> Adding driver loading at boot into '/etc/modules'"

list=$(grep NovaLPDrv /etc/modules)
if [[ $list = "" ]] ; then
    $SUDO sh -c 'echo NovaLPDrv >> /etc/modules'
fi

echo -e "=> Installation done\n\n""$RED""WARNING : ""$DEFAULT""Driver must be reinstalled if kernel version change\n"
echo "You need to reboot or execute these commands :"
echo -e "\tsudo rmmod ./NovaLPDrv.ko : To unload the old one, if he is loaded"
echo -e "\tsudo insmod ./NovaLPDrv.ko : To load the new driver"
echo "Then plug the launchpad, if having some rights issue on /dev/nlp0, then execute :"
ask="Do you want to reboot now ? [y/n]"
echo -e "\tsudo chmod 666 /dev/nlp0"
while true ; do
    read -p $ask ans
    case $ans in
    [yY] ) $SUDO reboot
            break;;
    * )
            break;;
    esac
done

IFS=$OLDIFS
