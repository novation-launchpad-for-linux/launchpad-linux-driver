/*
 *  This driver allows you to communicate with your Novation launchpad (NVLPD01)
 *  Copyright (C) 2012-2018 Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This driver is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This driver is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this driver.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ----------------------- INCLUDE ------------------------ */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <linux/mutex.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/ioport.h>
#include <linux/tty.h>
#include <linux/kref.h>
#include <linux/uaccess.h>

#include <linux/version.h>
#include <linux/usb.h>
#include <linux/poll.h>

#include "protocole.h"

/* ----------------------- MODULE ------------------------ */
MODULE_AUTHOR("Vincent Deca. known as Bigcake@ubuntu-fr.org");
MODULE_DESCRIPTION("Driver v1.0 for the Novation launchpad (P/N : NOVLPD01)");
MODULE_SUPPORTED_DEVICE("Novation launchpad 1235:000e");
MODULE_LICENSE("GPL");

/* ----------------------- DEFINE ------------------------ */
#define DRIVER_VERSION          "1.0:180101"  /* Version du driver */
#define VERSION_BYTE1           1             /* version avant la virgule */
#define VERSION_BYTE2           0             /* version après la virgule */

#define VENDOR_ID               0x1235        /* ID Novation  */
#define PRODUCT_ID              0x000e        /* ID Launchpad */

#define SIZE_BUFFER_DATA        250           /* Buffer tampon des évènements sur le périphérique*/
#define SIZE_BUFFER_URB_WRITE   8             /* Buffer d'écriture sur le périphérique */
#define NUMBER_MAX_CONNECTED    254           /* Nombre maximum de launchpad connectés */

#ifdef CONFIG_USB_DYNAMIC_MINORS
 #define BND_MINOR_BASE          0
#else
 #define BND_MINOR_BASE          96
#endif

#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 37)
 #define usb_free_coherent      usb_buffer_free
 #define usb_alloc_coherent     usb_buffer_alloc
#endif

/* s_drv.Flags */
#define FLG_URB_FILL          (1 << 0)       /* Fill() et submit() à faire lors d'un open() */
#define FLG_DRV_IN_USE        (1 << 1)       /* Un périphérique USB est connecté et utilise le driver */
#define FLG_DRV_IS_OPEN       (1 << 2)       /* Une application utilise un périphérique */
#define FLG_DRV_UNPLUG_DATA   (1 << 3)       /* Des données sont dans le buffer pour signaler le débranchement */

//#define DEBUG(val)       printk(KERN_INFO "NLP: %s : %s\n", __FUNCTION__, val);
//#define DEBUG(val)       ;

/* ----------------------- STRUCTURE ------------------------ */
/* Structure du périphérique */
typedef struct          NovationLaunchPad
{
  int                   Flags;         /* Sauvegarde de paramètres */
  unsigned char         Minor;         /* Sauvegarde du minor */
  char                  *DataBuff;     /* Buffer contenant les données envoyé par le periph usb */
  unsigned char         DataBuffPos;   /* Position du pointeur dans DataBuff */
  struct usb_endpoint_descriptor *InEndpoint;  /* IN endpoint descriptor */
  struct usb_endpoint_descriptor *OutEndpoint; /* OUT endpoint descriptor */
  struct usb_device     *udev;         /* device */
  struct usb_interface  *intf;         /* interface */
  struct usb_anchor     Anchor;        /* Anchor */
  struct semaphore      Lock;          /* Semaphore */
  struct urb            *UrbIn;        /* IN Interrupt URB */
  char                  *InBuff;       /* Buffer du URB IN */
  int                   Error;         /* Erreur */
  spinlock_t            LockError;     /* Verrou d'accès a l'erreur */
  wait_queue_head_t     Queue;         /* Pour la mise en attente du select */
  unsigned char         Stat;          /* Dernière état du launchpar 144 ou 176 */
  unsigned char         NbStatusError; /* Nombre d'erreurs du status affichées, limite a 10 pour ne pas flooder le syslog */
}                       s_nlp;

/* Structure générale du driver */
typedef struct          BND_driver
{
  s_nlp                 **Device;      /* Tableau de structure de périphérique */
  unsigned char         MaxConnected;  /* Nombre maximum de launchpad connecté en même temps, permet de déduire la place disponible dans s_drv.Device */
}                       s_drv;

s_drv                   *bnd = NULL;  /* Structure principale */

static struct usb_driver bnd_driver;
static struct usb_device_id id_table [] = {
  { USB_DEVICE(VENDOR_ID, PRODUCT_ID) },
  { },
};
MODULE_DEVICE_TABLE(usb, id_table);
static DEFINE_MUTEX(disconnect_mutex);

/* --------------------- DECLARATION ----------------------- */
static ssize_t WriteIt(struct file *file, const char __user *user_buff, size_t count, loff_t *ppos);
static ssize_t SendIt(s_nlp *dev, const char __user *user_buff, size_t count);

/* ----------------------- FUNCTION ------------------------ */
/* Affichage de l'erreur */
static void PrintError(s_nlp *dev, const char *func, const int status) {
  if (dev->NbStatusError < 10) {
    switch (-status) {
      //case EAGAIN: printk(KERN_INFO "NLP: Status error returned by %s() : EAGAIN (%d)\n", func, EAGAIN); break;
    case ECONNRESET: printk(KERN_INFO "NLP: Status error returned by %s() : ECONNRESET (%d)\n", func, ECONNRESET); break;
    case ECOMM: printk(KERN_INFO "NLP: Status error returned by %s() : ECOMM (%d)\n", func, ECOMM); break;
    case EILSEQ: printk(KERN_INFO "NLP: Status error returned by %s() : EILSEQ (%d)\n", func, EILSEQ); break;
    case EINPROGRESS: printk(KERN_INFO "NLP: Status error returned by %s() : EINPROGRESS (%d)\n", func, EINPROGRESS); break;
    case EINVAL: printk(KERN_INFO "NLP: Status error returned by %s() : EINVAL (%d)\n", func, EINVAL); break;
      //case EFBIG: printk(KERN_INFO "NLP: Status error returned by %s() : EFBIG (%d)\n", func, EFBIG); break;
      //case EHOSTUNREACH: printk(KERN_INFO "NLP: Status error returned by %s() : EHOSTUNREACH (%d)\n", func, EHOSTUNREACH); break;
      //case EMSGSIZE: printk(KERN_INFO "NLP: Status error returned by %s() : EMSGSIZE (%d)\n", func, EMSGSIZE); break;
    case ENODEV: printk(KERN_INFO "NLP: Status error returned by %s() : ENODEV (%d)\n", func, ENODEV); break;
      //case ENOENT: printk(KERN_INFO "NLP: Status error returned by %s() : ENOENT (%d)\n", func, ENOENT); break;
      //case ENOEXEC: printk(KERN_INFO "NLP: Status error returned by %s() : ENOEXEC (%d)\n", func, ENOEXEC); break;
      //case ENOMEM: printk(KERN_INFO "NLP: Status error returned by %s() : ENOMEM (%d)\n", func, ENOMEM); break;
      //case ENOSPC: printk(KERN_INFO "NLP: Status error returned by %s() : ENOSPC (%d)\n", func, ENOSPC); break;
    case ENOSR: printk(KERN_INFO "NLP: Status error returned by %s() : ENOSR (%d)\n", func, ENOSR); break;
      //case ENXIO: printk(KERN_INFO "NLP: Status error returned by %s() : ENXIO (%d)\n", func, ENXIO); break;
    case EOVERFLOW: printk(KERN_INFO "NLP: Status error returned by %s() : EOVERFLOW (%d)\n", func, EOVERFLOW); break;
      //case EPERM: printk(KERN_INFO "NLP: Status error returned by %s() : EPERM (%d)\n", func, EPERM); break;
    case EPIPE: printk(KERN_INFO "NLP: Status error returned by %s() : EPIPE (%d)\n", func, EPIPE); break;
    case EPROTO: printk(KERN_INFO "NLP: Status error returned by %s() : EPROTO (%d)\n", func, EPROTO); break;
    case EREMOTEIO: printk(KERN_INFO "NLP: Status error returned by %s() : EREMOTEIO (%d)\n", func, EREMOTEIO); break;
    case ESHUTDOWN: printk(KERN_INFO "NLP: Status error returned by %s() : ESHUTDOWN (%d)\n", func, ESHUTDOWN); break;
    case ETIME: printk(KERN_INFO "NLP: Status error returned by %s() : ETIME (%d)\n", func, ETIME); break;
    case ETIMEDOUT: printk(KERN_INFO "NLP: Status error returned by %s() : ETIMEDOUT (%d)\n", func, ETIMEDOUT); break;
    case EXDEV: printk(KERN_INFO "NLP: Status error returned by %s() : EXDEV (%d)\n", func, EXDEV); break;
    default: printk(KERN_INFO "NLP: Status error returned by %s() : %d\n", func, status); break;
    }
    ++dev->NbStatusError;
    if (dev->NbStatusError == 10) {
      printk(KERN_INFO "NLP: 10 status errors printed, disabling status error's printing for this device\n");
    }
  }
}

/* Libération des ressources prises par une structure s_bnd  */
static void FreeNLP(s_nlp *dev) {
  if (dev->UrbIn) {
    usb_free_urb(dev->UrbIn);
  }

  if (dev->DataBuff) {
    kfree(dev->DataBuff);
  }

  if (dev->InBuff) {
    kfree(dev->InBuff);
  }

  kfree(dev);
}

/* SendIt Callback */
static void CallbackSend(struct urb *urb)
{
  s_nlp *dev = urb->context;

  /* Vérification du status */
  if (urb->status) {
    PrintError(dev, __FUNCTION__, urb->status);
    if (urb->status == -ENOENT || urb->status == -ECONNRESET || urb->status == -ESHUTDOWN) {
      spin_lock(&dev->LockError);
      dev->Error = urb->status;
      spin_unlock(&dev->LockError);
    }
  }
  /* Libération du buffer */
  usb_free_coherent(urb->dev, urb->transfer_buffer_length, urb->transfer_buffer, urb->transfer_dma);
  up(&dev->Lock);
}

/* Envoi d'instructions au périphérique */
static ssize_t SendIt(s_nlp *dev, const char __user *user_buff, size_t count) {
  int ret;
  struct urb *urb = NULL;
  char *buff = NULL, flg;
  size_t to_send;

  /* Vérification de l'erreur */
  spin_lock(&dev->LockError);
  if ((ret = dev->Error) < 0) {
    dev->Error = 0;
    ret = (ret == -EPIPE) ? ret : -EIO;
  }
  spin_unlock(&dev->LockError);
  if (ret < 0) {
    up(&dev->Lock);
    printk(KERN_INFO "NLP: Error detected by %s() : %d\n", __FUNCTION__, ret);
    return (ret);
  }
  /* Allocation des ressources */
  if (!(urb = usb_alloc_urb(0, GFP_KERNEL))) {
    printk(KERN_INFO "NLP: Error returned by usb_alloc_urb()\n");
    up(&dev->Lock);
    return (-ENOMEM);
  }
  if (!(buff = usb_alloc_coherent(dev->udev, count, GFP_KERNEL, &urb->transfer_dma))) {
    printk(KERN_INFO "NLP: Error returned by usb_alloc_coherent()\n");
    usb_free_urb(urb);
    up(&dev->Lock);
    return (-ENOMEM);
  }
  /* Récupération du buffer utilisateur */
  if (copy_from_user(buff, user_buff, count)) {
    printk(KERN_INFO "NLP: Error returned by copy_from_user()\n");
    usb_free_coherent(dev->udev, count, buff, urb->transfer_dma);
    usb_free_urb(urb);
    up(&dev->Lock);
    return (-EFAULT);
  }
  to_send = count;

  /* Vérification du buffer, pour voir s'il s'agit d'une option */
  for (flg = 0, ret = 0; ret < count; ++ret) {
    if (buff[ret] == (char) LP_OPTION) {
      ++ret;
      if (ret < count) {
        if (buff[ret] == LP_GET_STAT) {
          ++ret;
          if (dev->DataBuffPos + 3 < SIZE_BUFFER_DATA) {
            dev->DataBuff[dev->DataBuffPos++] = (char) LP_OPTION;
            dev->DataBuff[dev->DataBuffPos++] = LP_GET_STAT;
            dev->DataBuff[dev->DataBuffPos++] = (char)dev->Stat;
          }
          else {
            printk(KERN_INFO "NLP: Too many data in queue: option LP_GET_STAT dropped\n");
          }
        }
        else if (buff[ret] == LP_GET_VERSION) {
          ++ret;
          if (dev->DataBuffPos + 4 < SIZE_BUFFER_DATA) {
            dev->DataBuff[dev->DataBuffPos++] = (char) LP_OPTION;
            dev->DataBuff[dev->DataBuffPos++] = LP_GET_VERSION;
            dev->DataBuff[dev->DataBuffPos++] = VERSION_BYTE1;
            dev->DataBuff[dev->DataBuffPos++] = VERSION_BYTE2;
          }
          else {
            printk(KERN_INFO "NLP: Too many data in queue: option LP_GET_VERSION dropped\n");
          }
        }
        else {
          goto end_SendIt;
        }

        flg = 1;
        if (ret < to_send) {
          memcpy(&buff[ret - 2], &buff[ret], to_send - ret);
        }
        to_send -= 2;
      }
      else {
        goto end_SendIt;
      }
    }
  }

  if (flg) {
    wake_up(&dev->Queue);
  }

  if (!to_send) {
    usb_free_coherent(dev->udev, count, buff, urb->transfer_dma);
    usb_free_urb(urb);
    up(&dev->Lock);
    return (count);
  }

  /* Envoi du buffer au périphérique */
  mutex_lock(&disconnect_mutex);
  usb_fill_int_urb(urb,
                   dev->udev,
                   usb_sndbulkpipe(dev->udev, dev->OutEndpoint->bEndpointAddress),
                   buff,
                   to_send,
                   CallbackSend,
                   dev,
                   10);

  urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;
  usb_anchor_urb(urb, &dev->Anchor);
  mb();
  ret = usb_submit_urb(urb, GFP_KERNEL);
  mutex_unlock(&disconnect_mutex);

  if (ret) {
    printk(KERN_INFO "NLP: SendIt(): Error %d returned by usb_submit_urb()\n", ret);
    usb_unanchor_urb(urb);
    usb_free_coherent(urb->dev,
                      urb->transfer_buffer_length,
                      urb->transfer_buffer,
                      urb->transfer_dma);
    up(&dev->Lock);
  }
  else {
    ret = count;
  }
  usb_free_urb(urb);
  return (ret);

  end_SendIt:
    printk(KERN_INFO "NLP: Invalid option message received (byte %d)\n", ret);
    usb_free_coherent(dev->udev, count, buff, urb->transfer_dma);
    usb_free_urb(urb);
    up(&dev->Lock);
    return (-EBADMSG);
}

/* Gestion du callback */
static void CallbackInterrupt(struct urb *urb) {
  s_nlp *dev = urb->context;
  int len;
  unsigned char *buff;

  /* Vérification du status */
  if (urb->status) {
    if (urb->status == -ENOENT || urb->status == -ECONNRESET || urb->status == -ESHUTDOWN) {
      return;
    }
    else {
      PrintError(dev, __FUNCTION__, urb->status);
    }
  }

  /* Mise en buffer des données envoyées par le launchpad */
  if ((len = urb->actual_length) > 0) {
    buff = urb->transfer_buffer;
    if (dev->Flags & FLG_DRV_IS_OPEN) {
      if (dev->DataBuffPos + len >= SIZE_BUFFER_DATA) {
        printk(KERN_INFO "NLP: Too many data in queue: last inputs dropped\n");
        if ((len = SIZE_BUFFER_DATA - dev->DataBuffPos)) {
          memcpy(&dev->DataBuff[dev->DataBuffPos], buff, len);
          dev->DataBuffPos += len;
        }
        len = urb->actual_length;
      }
      else {
        memcpy(&dev->DataBuff[dev->DataBuffPos], buff, len);
        dev->DataBuffPos += len;
      }
      /* Reveil de l'eventuel select() */
      wake_up(&dev->Queue);
    }

    for (--len; len >= 0; --len) {
      if (buff[len] == LP_MENU || buff[len] == LP_GRID) {
        dev->Stat = buff[len];
        break;
      }
    }
  }

  /* Resubmition du urb */
  if (dev->udev && (len = usb_submit_urb(dev->UrbIn, GFP_ATOMIC))) {
    printk(KERN_INFO "NLP: %s() : Resubmitting urb failed : (%d)\n", __FUNCTION__, len);
  }
}

/* Handle pour open() */
static int OpenLP(struct inode *inode, struct file *file) {
  struct usb_interface *intf;
  int minor;
  s_nlp *dev;

  minor = iminor(inode);
  mutex_lock(&disconnect_mutex);

  if (!(intf = usb_find_interface(&bnd_driver, minor))) {
    mutex_unlock(&disconnect_mutex);
    printk(KERN_INFO "NLP: %s() : Can't find device for minor %d\n", __FUNCTION__, minor);
    return (-ENODEV);
  }

  if (!(dev = usb_get_intfdata(intf))) {
    mutex_unlock(&disconnect_mutex);
    printk(KERN_INFO "NLP: %s() : Error returned by usb_get_intfdata()\n", __FUNCTION__);
    return (-ENODEV);
  }

  if (down_interruptible(&dev->Lock)) {
    mutex_unlock(&disconnect_mutex);
    printk(KERN_INFO "NLP: %s() : Error returned by down_interruptible()\n", __FUNCTION__);
    return (-ERESTARTSYS);
  }

  if (dev->Flags & FLG_DRV_IS_OPEN) {
    up(&dev->Lock);
    mutex_unlock(&disconnect_mutex);
    printk(KERN_INFO "NLP: %s() : Launchpad already in use, open() request rejected\n", __FUNCTION__);
    return (-EMFILE);
  }

  spin_lock(&dev->LockError);
  dev->Error = 0;
  spin_unlock(&dev->LockError);
  dev->Flags |= FLG_DRV_IS_OPEN;
  file->private_data = dev;
  up(&dev->Lock);
  mutex_unlock(&disconnect_mutex);
  return (0);
}

/* Handle pour close() */
static int ReleaseIt(struct inode *inode, struct file *file) {
  s_nlp *dev = 0;

  if (!(dev = file->private_data)) {
    printk(KERN_INFO "NLP: ReleaseIt() : dev not initialised\n");
    return (-ENODEV);
  }

  if (down_interruptible(&dev->Lock)) {
    printk(KERN_INFO "NLP: ReleaseIt() : Error returned by down_interruptible()\n");
    return (-ERESTARTSYS);
  }

  if (!dev->udev) {
    printk(KERN_INFO "NLP: Device unplugged before releasing file\n");
    up(&dev->Lock);
    return (-ENODEV);
  }

  dev->Flags &= ~(FLG_DRV_IS_OPEN);
  dev->DataBuffPos = 0;
  up(&dev->Lock);
  return (0);
}

/* Handle pour read() */
static ssize_t ReadIt(struct file *file, char __user *buff, size_t count, loff_t *off) {
  s_nlp *dev = file->private_data;
  int ret;

  /* Vérifications préalable & lock */
  if (down_interruptible(&dev->Lock)) {
    printk(KERN_INFO "NLP: ReadIt() : Error returned by down_interruptible()\n");
    return (-ERESTARTSYS);
  }

  if (count == 0) {
    up(&dev->Lock);
    return (0);
  }

  if (!dev->udev) {
    printk(KERN_INFO "NLP: ReadIt() : No device plugged\n");
    up(&dev->Lock);
    return (-ENODEV);
  }

  /* Ne lit pas plus que le nombre d'octets de données présent dans le buffer */
  if (count > dev->DataBuffPos) {
    count = dev->DataBuffPos;
  }

  /* Copie du résultat dans le buffer utilisateur */
  if ((ret = copy_to_user(buff, dev->DataBuff, count))) {
    printk(KERN_INFO "NLP: ReadIt() : %d octets couldn't be copied to user space\n", ret);
  }

  /* Déplace les données non lues du buffer */
  if (dev->DataBuffPos - count) {
    dev->DataBuffPos -= count;
    memcpy(dev->DataBuff, &dev->DataBuff[count], dev->DataBuffPos);
  }
  else {
    dev->DataBuffPos = 0;
  }

  up(&dev->Lock);
  return (count);
}

/* Handle pour write() */
static ssize_t  WriteIt(struct file *file, const char __user *user_buff, size_t count, loff_t *ppos) {
  s_nlp *dev = file->private_data;

  /* Recupère de 2 à 8 octets, tout le reste est invalide */
  if (count < 2 || count > SIZE_BUFFER_URB_WRITE) {
    return (-EINVAL);
  }

  /* Lock */
  if (!(file->f_flags & O_NONBLOCK)) {
    if (down_interruptible(&dev->Lock)) {
      return (-ERESTARTSYS);
    }
  }
  else if (down_trylock(&dev->Lock)) {
    return (-EAGAIN);
  }
  /* unlock est fait dans le call back du SendIt() */
  return (SendIt(dev, user_buff, count));
}

/* Gestion du select() */
static unsigned int PollIt(struct file *fd, struct poll_table_struct *poll) {
  unsigned int mask = 0;
  s_nlp *dev = fd->private_data;

  if (dev->DataBuffPos) {
    mask |= POLLIN | POLLRDNORM;
  }

  if (poll != NULL) {
    poll_wait(fd, &dev->Queue, poll);
  }

  return (mask);
}

static struct file_operations bnd_fops = {
  .owner =    THIS_MODULE,
  .write =    WriteIt,
  .read =     ReadIt,
  .poll =     PollIt,
  .open =     OpenLP,
  .release =  ReleaseIt,
};

static struct usb_class_driver bnd_class = {
  .name = "nlp%d",
  .fops = &bnd_fops,
  .minor_base = BND_MINOR_BASE,
};

/* Lancement lors du branchement USB */
static int ProbeLP(struct usb_interface *intf, const struct usb_device_id *id) {
  struct usb_device *udev;
  struct usb_host_interface *iface_desc;
  struct usb_endpoint_descriptor *endpoint;
  s_nlp *dev = NULL;
  int a;

  /* Conversion de la structure interface en structure device */
  if (!(udev = interface_to_usbdev(intf))) {
    printk(KERN_INFO "NLP: Error returned by interface_to_usbdev()\n");
    return (-ENODEV);
  }
  /* Recherche d"une  structure disponible pour le périphérique */
  for (a = 0; a < bnd->MaxConnected; ++a) {
    if (!bnd->Device[a]) {
      /* Initialisation de la structure s_nlp pour tout les launchpad qui seront connectés */
      if ((bnd->Device[a] = kzalloc(sizeof(s_nlp), GFP_KERNEL)) == NULL) {
        printk(KERN_INFO "NLP: Cannot allocate memory\n");
        return (-ENOMEM);
      }
      dev = bnd->Device[a];
      if (!(dev->DataBuff = kzalloc(SIZE_BUFFER_DATA, GFP_KERNEL))) {
        printk(KERN_INFO "NLP: Cannot allocate memory\n");
        FreeNLP(dev);
        return (-ENOMEM);
      }
      if (!(dev->UrbIn = usb_alloc_urb(0, GFP_KERNEL))) {
        printk(KERN_INFO "NLP: Error returned by usb_alloc_urb()\n");
        FreeNLP(dev);
        return (-ENOMEM);
      }

      #if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 37)
        sema_init(&dev->Lock, 1); /* 2.6.37 et plus */
      #else
        init_MUTEX(&dev->Lock);  /* avant 2.6.37 */
      #endif

      spin_lock_init(&dev->LockError);
      init_waitqueue_head(&dev->Queue);
      break;
    }
    else if (!(bnd->Device[a]->Flags & FLG_DRV_IN_USE)) {
      break;
    }
  }
  /* Augmentation du tableau de structure de launchpad si besoin */
  if (a == bnd->MaxConnected) {
    s_nlp **tmp;
    if (a == NUMBER_MAX_CONNECTED) {
      printk(KERN_INFO "NLP: Too many launchpad connected\n");
      return (-EAGAIN);
    }
    ++bnd->MaxConnected;
    if (!(tmp = kzalloc(sizeof(s_nlp *) * bnd->MaxConnected, GFP_KERNEL))) {
      printk(KERN_INFO "NLP: Cannot allocate memory\n");
      return (-ENOMEM);
    }
    memcpy(tmp, bnd->Device, sizeof(s_nlp *) * bnd->MaxConnected);
    kfree(bnd->Device);
    bnd->Device = tmp;
  }

  dev = bnd->Device[a];
  dev->udev = udev;
  dev->NbStatusError = 0;
  iface_desc = intf->cur_altsetting;
  /* Recherche des endpoint IN et OUT */
  for (a = 0; a < iface_desc->desc.bNumEndpoints; a++) {
    endpoint = &iface_desc->endpoint[a].desc;
    if (usb_endpoint_is_int_in(endpoint)) {
      dev->InEndpoint = endpoint;
    }
    else if (usb_endpoint_is_int_out(endpoint)) {
      dev->OutEndpoint = endpoint;
    }
  }

  if (!dev->InEndpoint) {
    printk(KERN_INFO "NLP: IN interrupt endpoint not found\n");
    return (-ENODEV);
  }

  if (!dev->OutEndpoint) {
    printk(KERN_INFO "NLP: OUT interrupt endpoint not found\n");
    return (-ENODEV);
  }

  if (dev->InBuff) {
    kfree(dev->InBuff);
  }

  if (!(dev->InBuff = kmalloc(le16_to_cpu(dev->InEndpoint->wMaxPacketSize), GFP_KERNEL))) {
    printk(KERN_INFO "NLP: Cannot allocate memory\n");
    return (-ENOMEM);
  }
  /* Sauvegarde du pointeur de la structure BND */
  usb_set_intfdata(intf, dev);
  /* Inscrit l'unité-USB dans le Noyau-USB */
  if (usb_register_dev(intf, &bnd_class)) {
    usb_set_intfdata(intf, NULL);
    kfree(dev->InBuff);
    dev->InBuff = 0;
    return (-ENODEV);
  }

  init_usb_anchor(&dev->Anchor);
  dev->Minor = intf->minor - BND_MINOR_BASE;
  dev->Flags &= ~(FLG_DRV_UNPLUG_DATA);
  dev->Stat = LP_MENU;
  dev->DataBuffPos = 0;
  usb_fill_int_urb(dev->UrbIn, dev->udev, usb_rcvintpipe(dev->udev, dev->InEndpoint->bEndpointAddress),
           dev->InBuff, le16_to_cpu(dev->InEndpoint->wMaxPacketSize),
           CallbackInterrupt, dev, dev->InEndpoint->bInterval);
  mb();

  if ((a = usb_submit_urb(dev->UrbIn, GFP_KERNEL))) {
    up(&dev->Lock);
    mutex_unlock(&disconnect_mutex);
    printk(KERN_INFO "NLP: %s() : Submitting int urb failed (%d)\n", __FUNCTION__, a);
    return (a);
  }

  dev->Flags |= FLG_DRV_IN_USE;
  printk(KERN_INFO "NLP: Launchpad attached to /dev/nlp%d\n", dev->Minor);
  return (0);
}

/* Libération des ressources à la déconnexion du périphérique */
static void DisconnectLP(struct usb_interface *intf) {
  s_nlp *dev = NULL;

  mutex_lock(&disconnect_mutex);
  dev = usb_get_intfdata(intf);
  printk(KERN_INFO "NLP: Novation Launchpad /dev/nlp%d disconnected\n", dev->Minor);
  usb_set_intfdata(intf, NULL);
  down(&dev->Lock);
  usb_deregister_dev(intf, &bnd_class);
  usb_kill_anchored_urbs(&dev->Anchor);

  if (dev->Flags & FLG_DRV_IS_OPEN) {
    spin_lock(&dev->LockError);
    dev->Error = -ENODEV;
    spin_unlock(&dev->LockError);
  }

  if (dev->Flags & FLG_DRV_IS_OPEN) {
    dev->Flags |= FLG_DRV_UNPLUG_DATA;
    dev->DataBuff[0] = LP_OPTION;
    dev->DataBuff[1] = LP_IS_UNPLUG;
    dev->DataBuffPos = 2;
    wake_up(&dev->Queue);
  }
  else {
    dev->DataBuffPos = 0;
  }

  dev->Flags &= ~(FLG_DRV_IN_USE | FLG_DRV_IS_OPEN);
  dev->Flags |= FLG_URB_FILL;
  up(&dev->Lock);
  mutex_unlock(&disconnect_mutex);
}

/* Structure usb_driver */
static struct usb_driver bnd_driver = {
  .name = "NovaLPDrv",
  .probe = ProbeLP,
  .disconnect = DisconnectLP,
  .id_table = id_table,
};

/* Chargement du module */
static int __init InitDrv(void) {
  int ret;

  printk(KERN_INFO "NLP: v"DRIVER_VERSION"\n");
  if (!(bnd = kzalloc(sizeof(s_drv), GFP_KERNEL))) {
    printk(KERN_INFO "NLP: Cannot allocate memory\n");
    return (-ENOMEM);
  }

  if (!(bnd->Device = kzalloc(sizeof(s_nlp *), GFP_KERNEL))) {
    printk(KERN_INFO "NLP: Cannot allocate memory\n");
    return (-ENOMEM);
  }

  bnd->MaxConnected = 1;
  if ((ret = usb_register(&bnd_driver))) {
    printk(KERN_INFO "NLP: usb_register failed (err : %d)\n", ret);
    return (ret);
  }

  return (0);
}

/* Déchargement du module */
static void __exit ReleaseDrv(void) {
  unsigned char a = 0;

  if (bnd) {
    for (a = 0; a < bnd->MaxConnected; ++a) {
      if (bnd->Device[a]) {
        FreeNLP(bnd->Device[a]);
      }
    }

    kfree(bnd->Device);
    kfree(bnd);
  }
  usb_deregister(&bnd_driver);
}

module_init(InitDrv);
module_exit(ReleaseDrv);
