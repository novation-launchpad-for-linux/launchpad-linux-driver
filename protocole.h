/*
 *  This driver allows you to communicate with your Novation launchpad (NVLPD01)
 *  Copyright (C) 2012-2018 Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 *  This driver is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This driver is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this driver.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INC_PROTOCOLE_H
#define INC_PROTOCOLE_H

/* ----------------- Fonctionnalités de mon driver ----------------------
 * ====== Récupération de l'état du launchpad ===== ok
 * Si une application envoi 2 octets {LP_OPTION, LP_GET_STAT}
 * Le driver répondra 3 octets {LP_OPTION, LP_GET_STAT, LP_MENU ou LP_GRID}
 *
 * ====== Récupération de la version du driver ===== ok
 * Si une application envoi 2 octets {LP_OPTION, LP_GET_VERSION}
 * Le driver répondra 4 octets {LP_OPTION, LP_GET_VERSION, version octet 1, version octet 2}
 * exemple pour la version 0.28 : octet 1 = 0, octet 2 = 28
 *
 * ====== Signal de débranchement du launchpad ===== ok
 * Si le launchpad est débranché alors qu'une application l'utilise
 * le driver enverra 2 octets {LP_OPTION, LP_IS_UNPLUG}
*/
/* -------------------- Protocole du Launchpad et du driver ----------------------- */
/* --- 1er octet --- */
#define LP_MENU        176  /* Les données suivantes correspondent aux boutons du menu du haut*/
#define LP_OPTION      175  /* Les données suivantes correspondent a une option du driver */
#define LP_GRID        144  /* Les données suivantes correspondent aux boutons de la grille ou de droite */

/* --- 2ème octet (ou 1er octet) --- */
#define LP_GET_STAT    1    /* 2ème octet - Option du driver - Récupération de l'état du launchpad */
#define LP_IS_UNPLUG   2    /* 2ème octet - Option du driver - Le launchpad a été débranché */
#define LP_GET_VERSION 3    /* 2ème octet - Option du driver - Récupération de la version du driver */

#define LP_TOP_LEARN   104  /* Bouton 'learn' */
#define LP_TOP_VIEW    105  /* Bouton 'view' */
#define LP_TOP_PAGE_L  106  /* Bouton '<]' */
#define LP_TOP_PAGE_R  107  /* Bouton '[>' */
#define LP_TOP_SESSION 108  /* Bouton 'session' */
#define LP_TOP_USER1   109  /* Bouton 'user 1' */
#define LP_TOP_USER2   110  /* Bouton 'user 2' */
#define LP_TOP_MIXER   111  /* Bouton 'mixer' */
#define LP_1X1         0    /* Bouton de la grille ligne 1 colonne 1 */
#define LP_1X8         7    /* Bouton de la grille ligne 1 colonne 8 */
#define LP_LEFT_VOL    8    /* Bouton 'vol' */
#define LP_2X1         16   /* Bouton de la grille ligne 2 colonne 1 */
#define LP_2X8         23   /* Bouton de la grille ligne 2 colonne 8 */
#define LP_LEFT_PAN    24   /* Bouton 'pan' */
#define LP_3X1         32   /* Bouton de la grille ligne 3 colonne 1 */
#define LP_3X8         39   /* Bouton de la grille ligne 3 colonne 8 */
#define LP_LEFT_SNDA   40   /* Bouton 'snd A' */
#define LP_4X1         48   /* Bouton de la grille ligne 4 colonne 1 */
#define LP_4X8         55   /* Bouton de la grille ligne 4 colonne 8 */
#define LP_LEFT_SNDB   56   /* Bouton 'snd B' */
#define LP_5X1         64   /* Bouton de la grille ligne 5 colonne 1 */
#define LP_5X8         71   /* Bouton de la grille ligne 5 colonne 8 */
#define LP_LEFT_STOP   72   /* Bouton 'stop' */
#define LP_6X1         80   /* Bouton de la grille ligne 6 colonne 1 */
#define LP_6X8         87   /* Bouton de la grille ligne 6 colonne 8 */
#define LP_LEFT_TRKON  88   /* Bouton 'trk on' */
#define LP_7X1         96   /* Bouton de la grille ligne 7 colonne 1 */
#define LP_7X8         103  /* Bouton de la grille ligne 7 colonne 8 */
#define LP_LEFT_SOLO   104  /* Bouton 'solo' */
#define LP_8X1         112  /* Bouton de la grille ligne 8 colonne 1 */
#define LP_8X8         119  /* Bouton de la grille ligne 8 colonne 8 */
#define LP_LEFT_ARM    120  /* Bouton 'arm' */
/* --- 3ème octet (ou 2ème octet) --- */
/* Ecriture  */
#define LP_COPY        (1 << 2)  /* Copie de la donnée dans le 2ème buffer */
#define LP_CLEAR       (1 << 3)  /* Efface le 2ème buffer */

#define LP_OFF         (LP_CLEAR | LP_COPY) /* On éteint */
#define LP_LOW_RED     (LP_OFF | 1 << 0)  /* Rouge faible */
#define LP_MED_RED     (LP_OFF | 1 << 1)  /* Rouge moyen */
#define LP_FULL_RED    (LP_LOW_RED | LP_MED_RED)  /* Rouge fort */

#define LP_LOW_GREEN   (LP_OFF | 1 << 4)  /* Vert faiblre */
#define LP_MED_GREEN   (LP_OFF | 1 << 5)  /* Vert moyen */
#define LP_FULL_GREEN  (LP_LOW_GREEN | LP_MED_GREEN)  /* Vert fort */

#define LP_LOW_YELLOW  (LP_OFF | LP_LOW_GREEN | LP_LOW_RED) /* Jaune faible */
#define LP_MED_YELLOW  (LP_OFF | LP_MED_GREEN | LP_MED_RED) /* Jaune moyen */
#define LP_FULL_YELLOW (LP_FULL_GREEN | LP_FULL_RED) /* Jaune fort */

#define LP_LOW_ORANGE  (LP_OFF | LP_MED_RED | LP_LOW_GREEN) /* Orange faible */
#define LP_MED_ORANGE  (LP_OFF | LP_FULL_RED | LP_MED_GREEN) /* Orange moyen */
#define LP_FULL_ORANGE (LP_FULL_RED | LP_LOW_GREEN) /* Orange fort*/

#define LP_START_FLASH 40 /* Mode de clignotement ON*/
#define LP_STOP_FLASH  48 /* Mode de clignotement OFF*/

/* Lecture  */
#define LP_PUSHED      127 /* Le bouton est appuié */
#define LP_RELEASE     0   /* Le bouton a été relaché */

/* ----------------- Autres infos ------------------- */

/* WRITE - 144, key, velocity */
#define LP_BRIGHT_LVL0   0  /* Eteint */
#define LP_BRIGHT_LVL1   1  /* faible */
#define LP_BRIGHT_LVL2   2  /* medium */
#define LP_BRIGHT_LVL3   3  /* full */

#define LP_CTRL_LED_CPY   (1 << 2) /* écrit les données de la LED dans les 2 buffer */
#define LP_CTRL_LED_CLR   (2 << 2) /* efface le buffer de copie de la LED  */

#define LP_RESET          {176, 0, 0}   /* Eteint tout */
#define LP_ALL_ON_L       {176, 0, 125} /* Allume tout, low */
#define LP_ALL_ON_M       {176, 0, 126} /* Allume tout, medium */
#define LP_ALL_ON_F       {176, 0, 127} /* Allume tout, full */

#endif /* INC_PROTOCOLE_H */
