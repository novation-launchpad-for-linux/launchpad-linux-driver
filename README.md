# Novation Launchpad Linux Driver

## Preamble

This project is a fork of Novation Launchpad Linux driver found at https://sourceforge.net/projects/drivernovationl/ and initially developed by Vincent Deca. known as Bigcake@ubuntu-fr.org.

Useful links:
- Original forums posts [FR]: https://forum.ubuntu-fr.org/viewtopic.php?id=1043581
- Original blog posts [EN]: https://videca.wordpress.com/2012/09/24/endriver-for-novation-launchpad/
- Original blog posts [FR]: https://videca.wordpress.com/2012/09/11/fr-nlp-driver/

## Instructions

1) Français
2) English

### Français

#### Installation

- Exécuter la commande 'make'
=> un fichier 'NovaLPDrv.ko' doit être créé
- Exécuter la commande 'make install' en root ou avec sudo
- Redémarrer le poste

Lors du branchement du launchpad, **/dev/nlp0** devrait apparaitre avec les droits écriture/lecture pour tout le monde

**ATTENTION** : lors d'une montée de version de votre kernel, il faudra recommencer cette procédure.

#### Dépannage
Lors du branchement du launchpad si aucun /dev/nlp n'apprait, suivre la procédure suivante :
- Démarrage du poste
- Chargement du driver : (ne pas brancher le launchpad tant que le driver n'est pas chargé !)
'sudo insmod /lib/modules/$(uname -r)/kernel/drivers/usb/misc/NovaLPDrv.ko'
- Branchement du launchpad (normalement nlp0 doit apparaitre dans /dev)
- Changement des droits pour la lecture/écriture pour tous sur le launchpad :
'sudo chmod 666 /dev/nlp0'

#### Désinstallation
- Exécuter la commande 'make remove'

#### Pour les développeurs
- Pour récupérer le dernier état (144 ou 176) du launchpad, coté matériel
  L'application doit envoyer 2 octets {LP_OPTION, LP_GET_STAT}
  Le driver répondra 3 octets {LP_OPTION, LP_GET_STAT, LP_MENU ou LP_GRID}
  
- Pour récupérer la version du driver 
  L'application doit envoyer 2 octets {LP_OPTION, LP_GET_VERSION}
  Le driver répondra 4 octets {LP_OPTION, LP_GET_VERSION, octet 1, octet 2}
  exemple pour la version 0.28 : octet 1 = 0, octet 2 = 28
  
- Lorsque le launchpad est débranché : 
  Le driver enverra 2 octets {LP_OPTION, LP_IS_UNPLUG}

### English

#### Installation
- Execute the command 'make'
=> a file 'NovaLPDrv.ko' must be created
- Execute the commande 'make install' when root or with sudo
- Reboot computer

When you plug your launchpad, **/dev/nlp0** should appear with read and write rights for all.

**WARNING**: when you upgrade your kernel, you'll have to restart this procedure.

#### Troubleshooting
When plugging the launchpad, if no /dev/nlpl appears try this procedure :
- Start your computer
- Load the driver: (don't plug the launchpad before driver is loaded)
'sudo insmod /lib/modules/$(uname -r)/kernel/drivers/usb/misc/NovaLPDrv.ko'
- Plug the launchpad (nlp0 should appears into /dev)
- Change read/write rights for everyone on the launchpad :
'sudo chmod 666 /dev/nlp0'

#### Uninstall
- Execute the command 'make remove'

#### For developpers
- To retrieve the last state (144 or 176) of the launchpad, hardware side
  The software must send 2 bytes {LP_OPTION, LP_GET_STAT}
  The driver will answer 3 bytes {LP_OPTION, LP_GET_STAT, LP_MENU or LP_GRID}

- To retrieve the driver version
  The software must send 2 bytes {LP_OPTION, LP_GET_VERSION}
  The driver will answer 4 bytes {LP_OPTION, LP_GET_VERSION, byte 1, byte 2}
  example for 0.28 version : byte 1 = 0, byte 2 = 28

- When launchpad is unplug :
  The driver will send 2 bytes {LP_OPTION, LP_IS_UNPLUG}
