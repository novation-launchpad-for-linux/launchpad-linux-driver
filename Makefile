NAME = novalpdrv
VERSION = 1.0

obj-m += NovaLPDrv.o

default:
	make CONFIG_MODULE_SIG=n -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

load:
	sudo insmod ./NovaLPDrv.ko

unload:
	sudo rmmod NovaLPDrv

install:
	chmod +x install.sh
	./install.sh

remove:
	@echo "Removing module from kernel/drivers/usb/misc"
	@rm -rf /lib/modules/`uname -r`/kernel/drivers/usb/misc/NovaLPDrv.ko
	@echo "********************"
	@echo "Updating kernel module list"
	@depmod
	@echo "********************"
	@echo "Removing launchpad plugging udev rules"
	@rm -rf /etc/udev/rules.d/10-persistent-NovaLPDrv.rules /etc/udev/rules.d/10-persistent-nlp.rules
	@sed -i '/NovaLPDrv/d' /etc/modules
	@echo "********************"
	@echo "Uninstall complete"
